from django.shortcuts import render
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status
from django.shortcuts import redirect, reverse
import datetime

# Create your views here.
def add_form(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            response = {}
            response['isiStatus'] = request.POST['isiStatus']
            form_item = Status(isiStatus=response['isiStatus'])
            form_item.save()
            return redirect(reverse('landingpage:view_status'))
    else:
        form = StatusForm()
    return render(request, 'add_form.html', {'title':'Status Form','form': form})


def view_status(request):
    data = Status.objects.all()
    return render(request, 'view_form.html', {'data':data})

def delete_status(request, pk):
    Status.objects.filter(pk = pk).delete()
    data = Status.objects.all()
    content = {'title':'Status', 'data':data}
    return render(request, 'view_form.html', {'data':data})
