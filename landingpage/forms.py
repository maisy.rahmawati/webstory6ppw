from django import forms
from .models import Status
from django.forms import ModelForm

class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ['isiStatus']
        labels = {'isiStatus':'Status'}
        widgets = {'isiStatus':forms.TextInput(attrs={'class':'form-control','type':'text'}),}
