from django.test import TestCase, Client
from django .http import HttpRequest
from .views import add_form, view_status, delete_status
from selenium import webdriver
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek alamat url pengisian status telah ada
    def test_url_address_add_form_exist(self):
        response = Client().get('/add_form/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url view status telah ada
    def test_url_address_view_status_exist(self):
        response = Client().get('/view_status/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url delete status telah ada
    def test_url_address_delete_status_exist(self):
        response = Client().get('/delete_status/1')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url pengisian status tidak ada
    def test_url_address_add_form_not_exist(self):
        response = Client().get('/add_form')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek alamat url view status tidak ada
    def test_url_address_view_status_not_exist(self):
        response = Client().get('/view_status')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek alamat url delete status tidak ada
    def test_url_address_delete_status_not_exist(self):
        response = Client().get('/delete_status/')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi add_form telah memiliki isi
    def test_add_form_is_written(self):
        self.assertIsNotNone(add_form)

    #Test untuk cek fungsi view_status telah memiliki isi
    def test_view_status_is_written(self):
        self.assertIsNotNone(view_status)

    #Test untuk cek fungsi add_form telah memiliki isi
    def test_delete_status_is_written(self):
        self.assertIsNotNone(delete_status)

    #Test untuk cek apakah template html telah digunakan pada halaman add_form
    def test_template_html_add_form_is_exist(self):
        response = Client().get('/add_form/')
        self.assertTemplateUsed(response, 'add_form.html')

    #Test untuk cek apakah template html telah digunakan pada halaman view_status
    def test_template_html_view_form_is_exist(self):
        response = Client().get('/view_status/')
        self.assertTemplateUsed(response, 'view_form.html')

    #Test untuk cek apakah string 'Hallo, apa kabar?' telah ada dihalaman add_form
    def test_content_string_in_html_add_form_is_exist(self):
        response = Client().get('/add_form/')
        self.assertContains(response, 'Hallo, apa kabar?')

class FunctionalTest(unittest.TestCase):
    #Melakukan setUp untuk starting web browser, yaitu dengan menggunakan google chrome
    def setUp(self):
        self.browser = webdriver.Chrome()

    #Melakukan test functionalnya ke website yang telah dibuat
    def test_functional_test(self):
        self.browser.get('https://maisy-webstory6ppw.herokuapp.com/add_form/')
        time.sleep(5) #Memberikan jeda waktu, yang sebenarnya website tidak melakukan apapun
        filling_box = self.browser.find_element_by_name('isiStatus')
        filling_box.send_keys('Coba Coba')
        filling_box.submit()
        time.sleep(10)
        self.assertIn("Coba Coba", self.browser.page_source)

if __name__ == '__main__':
   unittest.main(warnings='ignore')
