from django.db import models
import datetime

# Create your models here.
class Status(models.Model):
    isiStatus = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
